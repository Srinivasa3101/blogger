class User < ApplicationRecord
  has_one_attached :avatar
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

         validates_confirmation_of :password
         validates_presence_of :password_confirmation, :on => :create
         validates_uniqueness_of :email
         validates :username, presence: true
         validates :username, uniqueness: true, if: -> { self.username.present? }
         validates_uniqueness_of :username, case_sensitive: false
         validates_presence_of :username 

  has_many :blogs, dependent: :delete_all
end
