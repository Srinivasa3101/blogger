class BlogsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]

    def index
        @blogs = Blog.all
      end

    def show
        @blog = Blog.find(params[:id])
    end

    def new
        #@blog = Blog.new
        #@blog = current_user.blogs.build
        if current_user
          current_user.blogs.build
        else
          redirect_to root_path, alert: 'Login to create a new blog'
        end
    end

    def edit
      @blog = Blog.find(params[:id])
      if @blog.user_id != current_user.id
        redirect_to blogs_path
      end
    end

    def create
        #@blog = Blog.new(bloge)
        @blog = current_user.blogs.build(bloge)
 
        @blog.save
        redirect_to @blog
    end

    def update
        @blog = Blog.find(params[:id])
       
        if @blog.update(bloge)
          redirect_to @blog
        else
          render 'edit'
        end
    end

    def destroy
        @blog = Blog.find(params[:id])
        @blog.destroy
     
        redirect_to root_path
    end

    private
        def bloge
            params.require(:blog).permit(:title, :text, :blog_pics)
        end
end
