class WelcomeController < ApplicationController
  def index
    @blogs = Blog.all

    if current_user
      @user = current_user
    end
  end
end
