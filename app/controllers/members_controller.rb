class MembersController < Devise::RegistrationsController

    def create
        @user = User.new(user)

        if @user.save
            redirect_to new_user_session_path
        else
            flash[:error] = @user.errors
            redirect_to new_user_registration_path
        end

        

    end

    private
    def user
        params.require(:user).permit(:username, :email, :password, :password_confirmation, :avatar)
    end
end
