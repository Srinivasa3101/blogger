Rails.application.routes.draw do
  devise_for :users, :controllers => { :registrations => 'members' }

  get 'welcome/index'

  resources :blogs
  
  root 'welcome#index'
end
